var environment = process.env.NODE_ENV;

var config = {
  mode: "production",
  server: "",
  user_validate_endpoint: "/api/v1/user/validate",
  user_create_endpoint: "/api/v1/user",
  client_endpoint: "/api/v1/client"
};
if (environment === "development") {
  config["server"] = "https://localhost:44320";
} else {
  config["server"] = "https://cordial-api.com";
}
config["server"] = "https://cordial-api.com";

export default config;
