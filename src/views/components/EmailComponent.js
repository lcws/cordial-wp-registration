import React, { Component } from 'react';

class EmailComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: {},
            email: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.validate = this.validate.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value }, () => {

            //notify parent of change
            this.props.inputChanged(name, value);
        });


    }

    validate() {
        return new Promise((resolve, reject) => {
            const email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            let errors = {};

            if (this.state.email.match(email_regex) == null) {
                errors['email'] = "is-invalid";
                reject('email');
            }
            this.setState({ errors: errors }, () => resolve('email'));
        });
    }

    render() {
        const error_messages = this.state.errors;
        const email = this.state.email;
        return (
            <div className="form-group">
                <label className="form-control-label">Email address</label>
                <div className="input-group input-group-merge">
                    <div className="input-group-prepend">
                        <span className="input-group-text"><i className="fas fa-user"></i></span>
                    </div>
                    <input
                        type="email"
                        className={"form-control " + error_messages['email']}
                        id="input-email"
                        placeholder="name@example.com"
                        value={email}
                        name="email"
                        onChange={this.handleInputChange}
                    />
                    <div className="invalid-feedback">
                        Please enter a valid email address.
                                  </div>
                </div>
            </div>
        );
    }
}

export default EmailComponent;