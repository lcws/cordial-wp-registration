import React, { Component } from 'react';
import axios from 'axios';
import config from '../../config';

class WordpressUrlComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      wordpress_url: window.location.host,
      force_https: false
    };
    this.wp_rest_endpoint = "/wp-json/cordial/v1";
    this.handleInputChange = this.handleInputChange.bind(this);
    this.validate = this.validate.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({ [name]: value }, () => {

      //notify parent of change
      this.props.inputChanged(name, value);
    });
  }

  validate() {

    //make sure we can contact our Cordial WP endpoint
    let url = this.state.wordpress_url;

    //UX: get rid of any http:// prefix
    if (url.substr(0, 7) === "http://") {
      url = url.substr(7);
    }
    if (url.substr(0, 8) === "https://") {
      url = url.substr(8);
    }

    //UX: trim trailing / in URL if it exists
    if (url[url.length - 1] === '/') {
      url = url.substr(0, url.length - 1);
    }

    const formatted_url = url;
    this.setState({ wordpress_url: formatted_url });
    return new Promise((resolve, reject) => {

      //prepend with HTTP or HTTPS
      if (this.state.force_https === true) {
        url = "https://" + url;
      }
      else {
        url = "http://" + url;
      }

      //tack on REST endpoint
      url += this.wp_rest_endpoint;

      //ignore WP install when in developer mode
      if (config.mode === "developer") {
        resolve(true);
      }

      axios(url, {
        method: "get"
      })
        .then(response => {
          if (response !== null && response !== undefined) {
            const data = response.data;
            if (data.namespace !== null && data.namespace !== undefined) {
              this.setState({ errors: {} }, () => resolve(true));
            }
            else {
              reject(false);
            }
          }
          else {
            reject(false);
          }
        })
        .catch(err => {
          const errors = { wordpress_url: 'is-invalid' };
          this.setState({ errors: errors }, () => reject(false));
        });
    });
  }

  render() {
    const error_messages = this.state.errors;
    const { wordpress_url, force_https } = this.state;
    return (
      <React.Fragment>
        <div className="form-group">
          <label className="form-control-label" data-toggle="tooltip" title="The URL of your wordpress site.  If wordpress is hosted at the root level, this will just be your domain (e.g. mydomain.com)">Wordpress URL
                               &nbsp;<i className="fas fa-question-circle"></i>
          </label>
          <div className="input-group input-group-merge">
            <div className="input-group-prepend">
              <span className="input-group-text"><i className="fas fa-server"></i></span>
            </div>
            <input
              type="input"
              className={"form-control " + error_messages['wordpress_url']}
              id="input-domain"
              placeholder="cordial-api.com/wordpress"
              value={wordpress_url}
              name="wordpress_url"
              onChange={this.handleInputChange}
            />
            <div className="invalid-feedback">
              Could not detect a Wordpress install at this location.
                                  </div>
          </div>
        </div>
        <div className="my-4">
          <div className="custom-control custom-checkbox mb-3">
            <input
              type="checkbox"
              className="custom-control-input"
              id="check-https"
              checked={force_https}
              name="force_https"
              onChange={this.handleInputChange}
            />
            <label className="custom-control-label" htmlFor="check-https" data-toggle="tooltip" data-html="true" title="Using HTTPS is recommended but it requires that you have a <strong><u>valid</u></strong> SSL certificate.">Force HTTPS communication &nbsp;<i className="fas fa-question-circle"></i></label>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default WordpressUrlComponent;