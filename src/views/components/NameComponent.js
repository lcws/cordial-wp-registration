import React, { Component } from 'react';

class NameComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: {},
            first_name: "",
            last_name: ""
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.validate = this.validate.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({ [name]: value }, () => {

            //notify parent of change
            this.props.inputChanged(name, value);
        });


    }

    validate() {
        return new Promise((resolve, reject) => {
            let errors = {};
      
            if (this.state.first_name.length < 1) {
              errors['first_name'] = 'is-invalid';
            }
            if (this.state.last_name.length < 1) {
              errors['last_name'] = 'is-invalid';
            }
            if (Object.keys(errors).length > 0) {
              this.setState({ errors: errors }, () => {
                reject(false);
              });
            }
            else {
              this.setState({ errors: errors }, () => {
                resolve(true);
              });
            }
          });
    }

    render() {
        const error_messages = this.state.errors;
        const {first_name, last_name} = this.state;
        return (
            <React.Fragment>
                <div className="form-group">
                      <label className="form-control-label">First name</label>
                      <div className="input-group input-group-merge">
                        <div className="input-group-prepend">
                          <span className="input-group-text"><i className="fas fa-user"></i></span>
                        </div>
                        <input
                          type="text"
                          className={"form-control " + error_messages['first_name']}
                          id="input-first-name"
                          placeholder="John"
                          name="first_name"
                          value={first_name}
                          onChange={this.handleInputChange}
                        />
                        <div className="invalid-feedback">
                          Please enter your first name.
                                  </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <label className="form-control-label">Last name</label>
                      <div className="input-group input-group-merge">
                        <div className="input-group-prepend">
                          <span className="input-group-text"><i className="fas fa-user"></i></span>
                        </div>
                        <input
                          type="text"
                          className={"form-control " + error_messages['last_name']}
                          id="input-last-name"
                          placeholder="Doe"
                          name="last_name"
                          value={last_name}
                          onChange={this.handleInputChange}
                        />
                        <div className="invalid-feedback">
                          Please enter your last name.
                                  </div>
                      </div>
                    </div>
            </React.Fragment>
        );
    }
}

export default NameComponent;