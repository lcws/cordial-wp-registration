import LoadingIcon from '../../images/loading.gif';
import React, { Component } from 'react';

class RegisterButtonComponent extends Component {

    render() {
        const server_error_style = (this.props.server_error_message !== "" ? "block" : "none");
        const server_feedback_style = (this.props.server_feedback.length > 0 ? "block" : "none");
        const loading_icon_visible = this.props.waiting_on_server;
        const loading_icon_style = (loading_icon_visible === true) ? "visible" : "hidden";
        return (
            <div className="mt-4">
                <div className="row justify-content-center">
                    <div className="col-11">
                        <button disabled={loading_icon_visible} type="button" className={"btn btn-primary btn-block"} type="submit">
                            Receive API key &nbsp;&nbsp;
                            <img src={LoadingIcon} style={{visibility: loading_icon_style}} alt="loading" title="loading" />
                        </button>
                    </div>
                    <div className="col-1" style={{ display: loading_icon_style }} >

                    </div>

                    <div className="text-success" style={{ display: server_feedback_style, fontSize: "1.1rem", textAlign: "center" }}>
                    <ul>
                        {
                            this.props.server_feedback.map((value, index) => {
                                return(
                                    <li key={index}>{value}</li>
                                );
                            })
                        }
                        </ul>
                    </div>
                    <div className="invalid-feedback" style={{ display: server_error_style, fontSize: "1.1rem", textAlign: "center" }}>
                        {this.props.server_error_message}
                    </div>
                </div>

            </div>
        );
    }
}

export default RegisterButtonComponent;