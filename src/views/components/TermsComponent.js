import React, { Component } from 'react';

class TermsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      terms_agree: false,
      privacy_agree: false
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.validate = this.validate.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({ [name]: value }, () => {

      //notify parent of change
      this.props.inputChanged(name, value);
    });


  }

  validate() {
    return new Promise((resolve, reject) => {
      let errors = {};

      if (this.state.terms_agree === false) {
        errors['terms_agree'] = 'is-invalid';
      }
      /*
      if (this.state.privacy_agree === false) {
        errors['privacy_agree'] = 'is-invalid';
      }
      */
      if (Object.keys(errors).length > 0) {
        this.setState({ errors: errors }, () => {
          reject(false);
        });
      }
      else {
        this.setState({ errors: errors }, () => {
          resolve(true);
        });
      }
    });
  }

  render() {
    const error_messages = this.state.errors;
    const { terms_agree, privacy_agree } = this.state;
    return (
      <div className="my-4">
        <div className="custom-control custom-checkbox mb-3">
          <input
            type="checkbox"
            className={"custom-control-input " + error_messages['terms_agree']}
            id="check-terms"
            name="terms_agree"
            checked={terms_agree}
            onChange={this.handleInputChange}
          />
          <label className="custom-control-label" htmlFor="check-terms">I agree to the <a href="https://cordial-api.com/terms" target="_blank">Cordial API terms, conditions, and privacy policy</a></label>
          <div className="invalid-feedback">
            You must accept the terms, conditions, and privacy policy in order to use the Cordial API.
                      </div>
        </div>
      </div>
    );
  }
}

export default TermsComponent;