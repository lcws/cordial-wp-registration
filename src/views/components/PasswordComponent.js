import React, { Component } from 'react';

class PasswordComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      password: "",
      confirm_password: ""
    };

    this.show_confirm_password = this.props.confirm_password;
    if (this.show_confirm_password === undefined || this.show_confirm_password === null) {
      this.show_confirm_password = false;
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.validate = this.validate.bind(this);
    this.renderPassword = this.renderPassword.bind(this);
    this.renderConfirmPassword = this.renderConfirmPassword.bind(this);
    this.render = this.render.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({ [name]: value }, () => {

      //notify parent of change
      this.props.inputChanged(name, value);
    });


  }

  validate() {

    return new Promise((resolve, reject) => {
      let errors = {};

      if (this.state.password.length < 6) {
        errors['password'] = "is-invalid";
      }
      if (this.show_confirm_password === true && this.state.password !== this.state.confirm_password) {
        errors['confirm_password'] = "is-invalid";
      }
      
      if (Object.keys(errors).length > 0) {
        this.setState({ errors: errors }, () => {
          reject(false);
        });
      }
      else {
        this.setState({ errors: errors }, () => {
          resolve(true);
        });
      }
    });
  }

  renderPassword() {
    const error_messages = this.state.errors;
    const { password } = this.state;
    return (
      <div className="form-group mb-4">
        <label className="form-control-label" data-toggle="tooltip" title="Passwords must be at least 6 characters in length">Password
                               &nbsp;<i className="fas fa-question-circle"></i>
        </label>

        <div className="input-group input-group-merge">
          <div className="input-group-prepend">
            <span className="input-group-text"><i className="fas fa-key"></i></span>
          </div>
          <input
            type="password"
            className={"form-control " + error_messages['password']}
            id="input-password"
            placeholder="********"
            value={password}
            name="password"
            onChange={this.handleInputChange}
          />
          <div className="invalid-feedback">
            Passwords must be at least 6 characters in length.
                                  </div>
        </div>
      </div>
    );
  }

  renderConfirmPassword() {
    const error_messages = this.state.errors;
    const { confirm_password } = this.state;
    return (
      <div className="form-group">
        <label className="form-control-label">Confirm password</label>
        <div className="input-group input-group-merge">
          <div className="input-group-prepend">
            <span className="input-group-text"><i className="fas fa-key"></i></span>
          </div>
          <input
            type="password"
            className={"form-control " + error_messages['confirm_password']}
            id="input-password-confirm"
            placeholder="********"
            value={confirm_password}
            name="confirm_password"
            onChange={this.handleInputChange}
          />
          <div className="invalid-feedback">
            Passwords must match.
                                  </div>
        </div>
      </div>
    );
  }

  render() {
    const Password = this.renderPassword;
    const ConfirmPassword = this.renderConfirmPassword;
    if(this.show_confirm_password === true) {
      return(
        <React.Fragment>
          <Password />
          <ConfirmPassword />
        </React.Fragment>
      );
    }
    else {
      return(
        <Password />
      );
    }
  }
}

export default PasswordComponent;