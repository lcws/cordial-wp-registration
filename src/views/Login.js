import React, { Component } from "react";
import EmailComponent from './components/EmailComponent';
import PasswordComponent from './components/PasswordComponent'
import WordpressUrlComponent from './components/WordpressUrlComponent';
import Server from '../models/Server';
import RegisterButtonComponent from './components/RegisterButtonComponent'

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      server_error_message: "",
      api_key: "",
      server_feedback: [],
      is_server_active: false
    };

    this.email_ref = React.createRef();
    this.password_ref = React.createRef();
    this.wp_install_ref = React.createRef();

    this.components = {
      email: () => { return (<EmailComponent inputChanged={this.handleInputChange} ref={this.email_ref} />); },
      password: () => { return (<PasswordComponent inputChanged={this.handleInputChange} confirm_password={false} ref={this.password_ref} />); },
      wp_install: () => { return (<WordpressUrlComponent inputChanged={this.handleInputChange} ref={this.wp_install_ref} />); },
    };

    this.server = new Server();

    this.goToLogin = this.goToLogin.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.updateApiKey = this.updateApiKey.bind(this);
  }
  updateApiKey(key) {
    this.props.updateApiKey(key);
  }

  goToLogin() {
    this.props.loginFunction();
  }

  validateForm() {
    return new Promise((resolve, reject) => {
      this.email_ref.current.validate()
        .then(() => this.password_ref.current.validate())
        .then(() => {
          return this.wp_install_ref.current.validate();
        })
        .then(() => resolve(true))
        .catch(() => reject(false));
    });
  }

  handleInputChange(name, value) {
    this.setState({ [name]: value });
  }



  submitForm(evt) {
    evt.preventDefault();

    //validate once more on final submit
    this.validateForm()
      .then(() => {
        this.setState({ is_server_active: true }, () => {
          return new Promise((resolve, reject) => {
            resolve(true);
          })
        })
      })
      .then(() => this.server.authenticateUser(this.email_ref.current.state.email, this.password_ref.current.state.password))
      .then(() => this.server.registerClient(this.wp_install_ref.current.state.wordpress_url, this.wp_install_ref.current.state.force_https))
      .then((apiKey) => {
        this.setState({ 
          server_error_message: "API key is registered.  If this window does not automatically close, you may close it now.", 
          errors: {}, 
          api_key: apiKey,
          is_server_active: false }, () => {
          this.updateApiKey(apiKey);
        });
      })
      .catch((err) => {
        this.setState({ server_error_message: err, is_server_active: false });
      });
  }


  render() {
    const { api_key } = this.state;
    const components = this.components;
    const server_error_style = (this.state.server_error_message !== "" ? "block" : "none");
    return (
      <div className="main-content">
        <div className="container d-flex align-items-center">
          <div className="col py-5">
            <div className="row justify-content-center">
              <div className="col-md-8 col-lg-6 text-left">
                <div>
                  <div className="mb-5 text-center">
                    <p className="text-muted mb-0">Enter your credentials and domain information to receive an API key</p>
                    <span className="text-muted">Don't have a Cordial API account?&nbsp;</span>
                    <a href="#" onClick={this.goToLogin} className="font-weight-bold">Register here!</a>
                  </div>
                  <span className="clearfix"></span>
                  <form role="form" className="needs-validation" noValidate={true} onSubmit={this.submitForm}>
                    {Object.keys(components).map((key, index) => {
                      let Item = components[key];
                      return (
                        <Item key={key} />
                      );
                    })}

                    <RegisterButtonComponent
                      server_error_message={this.state.server_error_message}
                      server_feedback={this.state.server_feedback}
                      waiting_on_server={this.state.is_server_active}
                    />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
