import axios from 'axios';
import config from '../config';

class Server {

  constructor() {
    this.createAccount = this.createAccount.bind(this);
    this.authenticateUser = this.authenticateUser.bind(this);
    this.registerClient = this.registerClient.bind(this);
    this.auth_token = "";
    this.iv = "";
  }

  createAccount(email, password, first_name, last_name) {
    return new Promise((resolve, reject) => {
      const endpoint = config.server + config.user_create_endpoint;
      const payload = {
        FirstName: first_name,
        LastName: last_name,
        EmailAddress: email,
        Password: password
      };
      axios(endpoint, {
        method: "post",
        data: payload
      })
        .then(response => {
          if (response !== null && response !== undefined && response.data.userId !== null && response.data.userId > 0) {
            resolve(response.data);
          }
          else {
            reject(false);
          }
        })
        .catch((err) => {
          if (err.response === undefined || err.response === null) {
            err.response = { data: "Could not communicate with server" };
          }
          reject(err.response.data);
        });
    });
  }

  authenticateUser(email, password) {
    return new Promise((resolve, reject) => {

      const endpoint = config.server + config.user_validate_endpoint;
      axios(endpoint, {
        method: "post",
        data: { EmailAddress: email, Password: password }
      })
        .then(response => {
          if (response !== null && response !== undefined && response.data.userId !== null && response.data.userId > 0) {
            this.auth_token = response.headers["auth-token"];
            this.iv = response.headers["auth-token-iv"];
            resolve(response.data);
          }
          else {
            reject(false);
          }
        })
        .catch((err) => {
          if (err.response === undefined || err.response === null) {
            err.response = { data: "" };
          }
          reject(err.response.data);
        });
    });
  }

  registerClient(wordpress_url, force_https) {
    return new Promise((resolve, reject) => {
      let http_port = 80;
      if (force_https === true) {
        http_port = 443;
      }
      const endpoint = config.server + config.client_endpoint;
      const payload = {
        Domain: wordpress_url,
        UseHttps: force_https,
        HttpPort: http_port
      };
      axios(endpoint, {
        method: "post",
        data: payload,
        headers: {
          "auth-token": this.auth_token,
          "auth-token-iv": this.iv
        }
      })
        .then(response => {
          if (response !== null && response !== undefined && response.data.apiKey !== null) {
            resolve(response.data.apiKey);
          }
          else {
            resolve(false);
          }
        })
        .catch((err) => {
          if (err.response === undefined || err.response === null) {
            err.response = { data: "Could not communicate with server" };
          } else if (err.response.data === "") {
            err.response = { data: "This domain name is already registered with Cordial." };
          }
          reject(err.response.data);
        });
    });
  }
}

export default Server;