import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Register from './views/Register';
import Login from './views/Login';
import $ from 'jquery';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      has_account: false,
      api_key: "",
      context: props.context
    };

    this.logIn = this.logIn.bind(this);
    this.logOut = this.logOut.bind(this);
    this.updateApiKey = this.updateApiKey.bind(this);
  }

  updateApiKey(key) {
    this.setState({ api_key: key }, () => {
      let api_key_input = $('#api_key');
      if (api_key_input.length > 0) {
        api_key_input = api_key_input[0];
        if(api_key_input !== null) {
          api_key_input.value = this.state.api_key;
          
          //global function defined in static HTML
          window.closeRegistrationWindow();
        }
      }
    });
  }

  logIn() {
    this.setState({ has_account: true });
  }

  logOut() {
    this.setState({ has_account: false });
  }


  render() {
    const dev_mode = (process.env.NODE_ENV == "development") ? "DEV MODE" : "";
    if (this.state.has_account === true) {
      return (
        <div>
          {dev_mode}
          <Login loginFunction={this.logOut} updateApiKey={this.updateApiKey} />
        </div>
      );
    }
    else {
      return (
        <div>
          {dev_mode}
          <Register loginFunction={this.logIn} updateApiKey={this.updateApiKey} />
        </div>

      );
    }
  }
}

export default App;
